import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

public class App {
	
	static long start = System.currentTimeMillis();
    static int indice = 0;
    static int altura = 1;	
	public static void main(String[] args) {
		try {
			
			BufferedReader br = null;
			FileReader fr = null;

	        List<String> array = new ArrayList<String>();       

	        fr = new FileReader("caso50.txt");
	        br = new BufferedReader(fr);
	        
	        String currentLine;
	        
	        while ((currentLine = br.readLine()) != null) {
	        	array.add(currentLine);
	        	indice++;
	        }
	        
	        
	        String[] array2 = array.get(0).split(" "); 
	        String[] array3 = array.get(1).split(" ");
	        
	        String[] pre = new String[array2.length];
	        String[] inf = new String[array2.length];
	        
	        for (int j = 0; j < array2.length; j++) {
	        	pre[j] = new String(array2[j]);
	        	inf[j] = new String(array3[j]);
	        }
	        
	        indice = 0;
	        altura = arvore(pre, inf, 1);
	        
	        long end = System.currentTimeMillis();
	        
	        System.out.println(end - start);
	        System.out.println(altura);
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private static int arvore(String[] pre, String[] inf, int altura) {
		
		String nodo = pre[indice];
		
		int pos = 0;
		
		for (int j = 0 ; j < inf.length; j++) {
			if (inf[j].equals(nodo)) {
				pos = j;
				break;
			}
		}
		
		indice++;
		
		String[] esq = Arrays.copyOfRange(inf, 0, pos);
		String[] dir = Arrays.copyOfRange(inf, pos+1, inf.length);
		
		int dirA = 0;
		int esqA = 0;
		
		if (esq.length != 0) {
			esqA = arvore(pre, esq, altura+1);
		} else {
			return altura;
		}
		
		
		if (dir.length != 0) {
			dirA = arvore(pre, dir, altura+1);
		} else {
			return altura;
		}
		
		if (esqA > dirA) {
			return esqA;
		} else {
			return dirA;
		}
	}
}
